import docker 

client = docker.from_env()

print(client.containers.list()[0])
container = client.containers.get(client.containers.list()[0].name)
print(f"running container: {container}")
for line in container.logs(stream=True):
    print(line.strip())