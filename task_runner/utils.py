import os
import json
import datetime
import subprocess as sp
from pymongo import MongoClient
from celery import shared_task
from bson.json_util import dumps
from bson.objectid import ObjectId
from task_queues.settings import *
import threading
import yaml
from task_runner.tasks import *


def runInference(cmd):
    os.system(cmd)


def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class MongoHelper:
    client = None

    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=MONGO_HOST, port=MONGO_PORT)
        self.db = self.client["LIVIS"]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None, domain="livis"):  #
        _DB = "LIVIS"
        DB = self.client[_DB]
        # make it as cname in [LIST OF COMMON COLLECTIONS]
        if cname == 'permissions' or cname == 'domains':
            pass
        else:
            cname = domain + cname
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


@shared_task
def runDocker(domain_list):
    # print("Inside task")

    client = MongoHelper()

    for item in domain_list:
        collection = client.getCollection('experiment', domain=item)
        document = findEarliest(collection)

        if not (document):
            # print("No data")
            continue

        if document["model_type"] == "yolo":
            # print("starting training ", item)
            run_yolov5(document)
            return None
        elif document["model_type"] == "keras_retinanet":
            run_KerasRetinanet(document)
    return None


def findEarliest(mongo_collection):
    earliest = None
    #query = {"status": "Initialized"}
    query_db = {"$and": [{"status": "Initialized"},{"base_url":BASE_URL}]}
    cursor = mongo_collection.find(query_db)
    # logic for adding mount dir path in mongo db
    # for ele in cursor:
    #     exp_id = ele["_id"]
    mount_dir_path = MOUNT_DIR_PATH
    mongo_collection.find_and_modify(query=query_db,
                                     update={"$set": {'mount_data_path': mount_dir_path}},
                                     upsert=False, full_response=True)
    # end

    temp = [i for i in cursor]
    if len(temp) > 0:
        earliest = temp[0]
    for doc in temp:
        earliest_date = datetime.datetime.strptime(earliest["created_at"], "%m/%d/%Y, %H:%M:%S")
        doc_date = datetime.datetime.strptime(doc["created_at"], "%m/%d/%Y, %H:%M:%S")

        if earliest_date > doc_date:
            earliest = doc

    return earliest


def getGpuMemory():
    _output_to_list = lambda x: x.decode('ascii').split('\n')[:-1]

    ACCEPTABLE_AVAILABLE_MEMORY = 1024
    COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"
    memory_free_info = _output_to_list(sp.check_output(COMMAND.split()))[1:]
    memory_free_values = [int(x.split()[0]) for i, x in enumerate(memory_free_info)]
    #  print(memory_free_values)
    return memory_free_values


def run_training(cmd):
    os.system(cmd)


def run_KerasRetinanet(document):
    # print("running keras")
    exp_params = dumps(document)
    dict_a = document
    experiment_field = dict_a["_id"]
    # print(experiment_field)

    # create folders
    if not os.path.exists(MODELS_PATH):
        os.mkdir(MODELS_PATH)
    if not os.path.exists(EXPERIMENT_PATH):
        os.mkdir(EXPERIMENT_PATH)

    exp_id_folder_path = os.path.join(EXPERIMENT_PATH, str(experiment_field))

    if not os.path.exists(exp_id_folder_path):
        os.mkdir(exp_id_folder_path)

    mount_dir_path = MOUNT_DIR_PATH
    # os.system("chmod -R  777 " + str(exp_id_folder_path))
    parameters_file_path = os.path.join(exp_id_folder_path, "parameters.json")
    with open(parameters_file_path, "w") as f:
        json.dump(exp_params, f)

    cmd = "docker run --rm --gpus all -v  " + exp_id_folder_path + ":/var_data2 --network host" + "  --name" + "  " + str(
        experiment_field) + " " + "godzilla26/keras_retinanet:v1.23"
    # print("before os.system")
    os.system(cmd)
    # print("before subprocess")
    return 1


def run_yolov5(document):
    document["kafka_url"] = KAFKA_URL
    document["mongo_server_host"] = MONGO_HOST
    document["mongo_server_port"] = MONGO_PORT
    document["mongo_db_name"] = MONGO_DB
    document["mongo_collection_parts"] = MONGO_COLLECTION_PARTS
    document["redis_host"] = REDIS_HOST
    document["redis_port"] = REDIS_PORT
    document["datadrive_host"] = datadrive_host
    document["datadrive_port"] = datadrive_port
    document.pop("model_id")
    document.pop("threshold")
    hyperparams = document["hyperparameters"]
    hyperparams["task"] = "train"
    document["hyperparameters"] = hyperparams
    dict_a = document
    experiment_field = dict_a["_id"]

    # print("Running experiment " + str(experiment_field))
    if not os.path.exists(EXPERIMENT_PATH):
        os.mkdir(EXPERIMENT_PATH)

    exp_id_folder_path = os.path.join(EXPERIMENT_PATH, str(experiment_field))

    if not os.path.exists(exp_id_folder_path):
        os.mkdir(exp_id_folder_path)

    mount_dir_path = MOUNT_DIR_PATH
    # os.system("chmod -R  777 " + str(exp_id_folder_path))
    parameters_file_path = os.path.join(exp_id_folder_path, "parameters.json")

    document["exp_id"] = str(ObjectId(document["_id"]))
    document.pop("_id")
    with open(parameters_file_path, "w") as f:
        json.dump(document, f)

    outfile_yaml = os.path.join(exp_id_folder_path, "hyp.scratch.yaml")
    data = document["hyperparameters"]["hyperparameters_dict"]
    # data.pop("anchors")
    with open(outfile_yaml, 'w') as outfile_yaml:
        yaml.dump(data, outfile_yaml, default_flow_style=False)
    outfile_yaml.close()

    cmd = "docker run --rm --gpus all --shm-size=25g -v  " + exp_id_folder_path + ":/datadrive --network host --name " + str(
        document["exp_id"]) + "  "+DOCKER_VERSION
    os.system(cmd)
    return 1


def kill_stale_containers(domain):
    mp = MongoHelper()
    collection = mp.getCollection("experiment", domain=domain)
    docs = collection.find({'kill_containers_flag': True})
    docker_lis = []
    for doc in docs:
        docker_list = doc.get('kill_containers')
        for docker_name in docker_list:
            cmd = "docker container inspect -f '{{.State.Running}}' " + str(docker_name)
            status = os.system(cmd)
            if status == 0:
                # print("stopping container", docker_name)
                s = os.system("docker container stop " + str(docker_name))
            docker_list.remove(docker_name)
        doc['kill_containers'] = docker_list
        doc['kill_containers_flag'] = False
        collection.update_one({'_id': doc["_id"]}, {"$set": doc})
        # if os.path.isdir(EXPERIMENT_PATH + "/" + str(doc["_id"])):
        #     os.rmdir(EXPERIMENT_PATH + "/" + str(doc["_id"]))
    return 0


def restart_active_containers(domain):
    mp = MongoHelper()
    collection = mp.getCollection("experiment", domain=domain)
    docs = collection.find({"$and": [{"deployed": True}, {"container_deployed": True}]})
    for exps in docs:
        existing_containers_list = exps.get("running_containers", [])
        for docker_name in existing_containers_list:
            cmd = "docker container inspect -f '{{.State.Running}}' " + str(docker_name)
            status = os.system(cmd)
            if status != 0:
                # print("restart conatiner ", docker_name)
                exp_id_folder_path = EXPERIMENT_PATH + str(exps['_id']) + "/"
                if exps['inf_deployment'] == "gpu":
                    # print("@@@@@@@@@@ GPU  deployment")
                    docker_command = "docker run --rm --gpus all --shm-size=25g -v  " + exp_id_folder_path + ":/datadrive --network host --name " + docker_name + "  "+DOCKER_VERSION
                    t = threading.Thread(target=runInference, args=(docker_command,))
                    t.start()
                    # print(docker_command)
                else:
                    # print("@@@@@@@@@@ CPU deployment")
                    docker_command = "docker run --rm --shm-size=25g -v  " + exp_id_folder_path + ":/datadrive --network host --name " + docker_name + "  "+DOCKER_VERSION
                    t = threading.Thread(target=runInference, args=(docker_command,))
                    t.start()
                    # print(docker_command)













