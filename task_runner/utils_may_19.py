import os
import json
import redis
import datetime
import random
import subprocess as sp
import pymongo as mp 
from celery import shared_task
from bson.json_util import dumps
from bson.objectid import ObjectId
from task_queues import settings
import subprocess	
import threading

class MongoHelper:

    def __init__(self,host,port,db_name):

        self.client = mp.MongoClient(host=host,port=port)
        self.db = self.client[db_name]

    def getCollection(self,collection_name):
        return self.db[collection_name]


mongo_client = MongoHelper(host=settings.MONGO_HOST,port=settings.MONGO_PORT,db_name="LIVIS")
collection = mongo_client.getCollection(collection_name="experiment") 

def findEarliest(mongo_collection=collection):
    earliest = None
    query = {"status":"Initialized"}
    cursor = mongo_collection.find(query)
    #logic for adding mount dir path in mongo db 
    # for ele in cursor:
    #     exp_id = ele["_id"]
    mount_dir_path = settings.MOUNT_DIR_PATH
    mongo_collection.find_and_modify(query={"status":"Initialized"}, update={"$set": {'mount_data_path': mount_dir_path}}, upsert=False, full_response= True)
    # end 
    temp = [i for i in cursor]
    print(temp)
    if len(temp) > 0:
        earliest = temp[0]
    for doc in temp:
        earliest_date = datetime.datetime.strptime(earliest["created_at"],"%m/%d/%Y, %H:%M:%S")
        doc_date = datetime.datetime.strptime(doc["created_at"],"%m/%d/%Y, %H:%M:%S")

        if earliest_date > doc_date:
            earliest = doc 

    return earliest


def getGpuMemory():
  _output_to_list = lambda x: x.decode('ascii').split('\n')[:-1]

  ACCEPTABLE_AVAILABLE_MEMORY = 1024
  COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"
  memory_free_info = _output_to_list(sp.check_output(COMMAND.split()))[1:]
  memory_free_values = [int(x.split()[0]) for i, x in enumerate(memory_free_info)]
#   print(memory_free_values)
  return memory_free_values

def run_training(cmd):
    os.system(cmd)

@shared_task
def runDocker():
    print("Inside task")
    document= findEarliest(mongo_collection=collection)
    # print(document)
    if len(document) == 0:
        print("No data") 
        return None
    exp_params = dumps(document)
    # mp = document.find({})
    # for doc in mp:
    #     colle_id = doc['_id']    

    mount_dir_path = settings.MOUNT_DIR_PATH

    parameters_file_path = os.path.join(mount_dir_path,"parameters.json")
    with open(parameters_file_path,"w") as f:
        json.dump(exp_params,f) 


    dict_a = document
    experiment_field = dict_a["_id"]
    print(experiment_field)

    # cmd = "docker run --rm --gpus all -v /home/lincode/Desktop/livis_v2/republic/backend/LIVIS/livis/training/:/var_data2 -p "+ assigned_port+":6006  --network host object_detection_v20"

    # cmd = "docker run --rm --gpus all -v  "+mount_dir_path+":/var_data2 --network host object_detection_v51"
    cmd = "docker run --rm --gpus all -v  "+mount_dir_path+":/var_data2 --network host" +"  --name"+"  "+ str(experiment_field) +" " +"godzilla26/keras_retinanet:v19.0"
    # dict_a = document
    # experiment_field = dict_a["_id"]
    # print(experiment_field)
    # keras_retainanet_v9
    print("before os.system")
    # t1 = threading.Thread(target =run_training,args=(cmd,) )
    os.system(cmd)
    # t1.start()
    print("before subprocess")


   
    


