from bson import ObjectId
from background_task import background
from task_queues import settings
from task_queues.settings import *
from task_runner.utils import *
from background_task.models import Task
import docker
import threading
import json

# Task.objects.all().delete()
# Task.objects.filter(task_name__startswith=__name__).delete()

client = MongoHelper()
mp = MongoHelper().getCollection("domains")
doc = mp.find_one()
global domain_list
domain_list = doc.get('domains', ["livis"])
docker_client = docker.from_env()


@background(schedule=10)
def scheduler():
    #print("Starting")
    if getGpuMemory()[0] >= 1500:
        #print("Scheduling:", datetime.datetime.now())
        runDocker.delay(domain_list)
    for item in domain_list:
        serving_scheduler(item)
        kill_stale_containers(item)
        # restart_active_containers(item)


client = MongoHelper()


def runInference(cmd):
    os.system(cmd)


def serving_scheduler(domain):
    config = {}
    #print("Deployment Scheduler")
    exp_coll = client.getCollection("experiment", domain=domain)

    available_to_deploy = exp_coll.find({"$and": [{"deployed": True}, {"container_deployed": False}, {
        "deployed": {"$exists": True}}, {"container_deployed": {"$exists": True}}, {"base_url":BASE_URL}]})

    deploy_lists = [av for av in available_to_deploy]
    #print("\n\ndeploying ", len(deploy_lists), " experiments")
    for exps in deploy_lists:
        # download experiments folder from gcp datadrive
        # gsutil cp gs://livis_datadrive/+exp_id/ /datadrive/models/experiments/
        if not os.path.isdir(EXPERIMENT_PATH + "/" + str(exps["_id"])):
            os.mkdir(EXPERIMENT_PATH + "/" + str(exps["_id"]))
        cmd = "gsutil -m cp -r gs://livis_datadrive/" + str(exps["_id"]) + "/" + "  " + EXPERIMENT_PATH + "/"
        #print(cmd)
        os.system(cmd)
        ws_coll = client.getCollection("workstations", domain=domain)
        #print("Picking up experiment")
        topic = ""
        config["topic"] = ""
        deploy_dict = {}
        if exps.get("deploy_data"):
            deploy_dict = exps.get("deploy_data")
        camera_name = ""
        if exps.get("inference_mode") == "static":
            camera_name = "static"

        for key, value in deploy_dict.items():
            camera_name = value

            ws_id = key
            ws_cams = list(ws_coll.find({"_id": ObjectId(ws_id)})[0]["cameras"])
            ws_cam_ids = [cam["camera_id"] for cam in ws_cams]
            ws_cam_names = [cam["camera_name"] for cam in ws_cams]
            ws_name = ws_coll.find({"_id": ObjectId(ws_id)})[0]["workstation_name"]

            for i in range(0, len(ws_cam_names)):
                if ws_cam_names[i] == camera_name:
                    id = ws_cam_ids[i]
                    id = str(id)
                    id = id.replace(".", "")
                    id = id.replace(":", "")
                    topic = str(ws_id) + "_" + str(id)

                    break

            topic = topic.replace(":", "")
            topic = topic.replace(".", "")
            config["topic"] = topic

            if exps.get("inference_mode") == "mobile":
                # camera_name = "mobile"
                config["mobile_id"] = camera_name

            config["inf_path"] = exps.get("inference_graph")
            config["inference_mode"] = exps.get("inference_mode")
            #print('exps', exps)
            #print('config["inference_mode"]', config["inference_mode"])
            config['workstation_id'] = ws_id
            config['experiment_id'] = str(exps['_id'])
            model_type = exps["model_type"]
            exps["container_deployed"] = True
            exps["hyperparameters"]["confidence_threshold"] = 0.3

            docker_name = str(ws_name) + "_" + str(camera_name) + "_" + str(exps['part_id'])

            cmd = "docker container inspect -f '{{.State.Running}}' " + str(docker_name)
            status = os.system(cmd)
            if status == 0:
                #print("stopping container", docker_name)
                s = os.system("docker container stop " + str(docker_name))

            exps["container_state"] = "Loading"
            exps["container_deployed"] = True
            #print("****************")
            #print(exps["_id"])

            existing_containers_list = exps.get('running_containers', [])
            if docker_name not in existing_containers_list:
                existing_containers_list.append(docker_name)
            exps["running_containers"] = existing_containers_list

            exp_coll.update_one({'_id': exps["_id"]}, {"$set": exps})

            part_id = exps["part_id"]
            pp = client.getCollection("parts", domain=domain)
            part = pp.find_one({'_id': ObjectId(part_id)})
            kanban = part['kanban'] if 'kanban' in part else {}

            if kanban:
                defect_list = kanban["defects"]
                feature_list = kanban["features"]

            if model_type == "yolo":
                exp_id_folder_path = EXPERIMENT_PATH + "/" + str(config['experiment_id']) + "/"
                param_path = exp_id_folder_path + "parameters.json"

                with open(param_path) as file:
                    data = json.load(file)
 
                data["hyperparameters"]["task"] = "inference"
                data["inference_mode"] = config["inference_mode"]

                for key in config:
                    data[key] = config[key]

                if data.get("weights"):
                    data.pop("weights")
                data["trained_weights"] = "/datadrive/runs/train/exp/weights/best.pt"
                data["feature_list"] = feature_list
                data["defect_list"] = defect_list
                data['inf_deployment'] = exps['inf_deployment']
                data["kafka_url"] = KAFKA_URL
                data["mongo_server_host"] = MONGO_HOST
                data["mongo_server_port"] = MONGO_PORT
                data["mongo_db_name"] = MONGO_DB
                data["mongo_collection_parts"] = MONGO_COLLECTION_PARTS
                data["redis_host"] = REDIS_HOST
                data["redis_port"] = REDIS_PORT
                data["datadrive_host"] = datadrive_host
                data["datadrive_port"] = datadrive_port 

                with open(param_path, "w") as f:
                    json.dump(data, f)

                restart_policy = {"Name": "on-failure", "MaximumRetryCount": 5}
                if exps['inf_deployment'] == "gpu":
                    #print("@@@@@@@@@@ GPU  deployment")
                    docker_client.containers.run("ritikaniyengar/train_docker_iamge:latest",
                                                                device_requests=[docker.types.DeviceRequest(device_ids=["0"], capabilities=[['gpu']])],
                                                                network="host",
                                                                remove=True,
                                                                restart_policy=restart_policy,
                                                                shm_size="25G",
                                                                # volumes=""
                                                                )
                    # docker_command = "docker run --rm --gpus all --shm-size=25g -v  " + exp_id_folder_path + ":/datadrive --network host --name "+ docker_name + " "+DOCKER_VERSION
                    # t = threading.Thread(target=runInference, args=(docker_command,))
                    # t.start()
                    #print(docker_command)
                else:
                    docker_client.containers.run("ritikaniyengar/tain_docker_image:latest",
                                                                # device_requests=[docker.types.DeviceRequest(device_ids=["0"], capabilities=[['gpu']])],
                                                                network="host",
                                                                remove=True,
                                                                restart_policy=restart_policy,
                                                                shm_size="25G",
                                                                volumes="",
                                                                )