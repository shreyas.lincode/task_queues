from utils import MongoHelper
import cv2
import os
import csv 


image_dir = r"/home/lincode/Desktop/livis_v2/republic/backend/LIVIS/livis/training/image_data"
out_dir = r"/home/lincode/Desktop/livis_v2/republic/backend/LIVIS/livis/training/bb_img"
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
csv_path = r"/home/lincode/Desktop/livis_v2/republic/backend/LIVIS/livis/training/train_labels.csv"

csv_dict = {}

with open(csv_path) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for i in csv_reader:
        img = cv2.imread(os.path.join(image_dir,i[0]))
        img_name = i[0]
        xmin = str(i[4])
        # print(xmin)
        ymin = str(i[5])
        xmax = str(i[6])
        ymax= str(i[7])
        st = (int(xmin),int(ymin))
        ed = (int(xmax),int(ymax))
        if img_name in csv_dict:
            csv_dict[img_name].append((st, ed))
        else:
            csv_dict[img_name] = [(st, ed)]


mp = MongoHelper( host="localhost",port=27017,db_name="LIVIS").getCollection('6013f51df51ef4974e740533_dataset')
out_dir = '/home/lincode/Documents/Shyam/test_imgs/'
def get_annotation(id):
    pr = mp.find_one({'_id' : id})
    fp = pr['file_path']
    img_name = fp.split('/')[-1]
    img = cv2.imread(fp)
    #print(img)
    height, width,channel = img.shape
    #print(height, width)
    for anno in pr['annotation_detection']:
        x,y,dx, dy = anno['x'], anno['y'],anno['w'],anno['h']
        xmin = x * width
        ymin = y * height
        xmax = (x + dx) * width
        ymax = (y + dy) * height
        st = (int(xmin),int(ymin))
        ed = (int(xmax),int(ymax))
        c = (255, 0, 0)
        thickness = 3
        img = cv2.rectangle(img,st,ed,c,thickness)
        print("{} : : : {} , {} : : : {}".format(fp , st, ed, csv_dict[img_name])) 
    cv2.imwrite(os.path.join(out_dir,fp.split('/')[-1]),img)  


def run():
    pr = mp.find({'state' : 'tagged'})
    for p in pr:
        get_annotation(p['_id'])

run()
