import docker 
client = docker.from_env()

restart_policy = {"Name": "on-failure", "MaximumRetryCount": 5}
# container = client.containers.get()
print(f"running container: {client.containers.list()}")
client.containers.run("train_docker_image:v4",
                    device_requests=[docker.types.DeviceRequest(device_ids=["0"], capabilities=[['gpu']])],
                    # name="docker_train",
                    network="host",
                    remove=True,
                    restart_policy=restart_policy,
                    shm_size="25G",
                    )
